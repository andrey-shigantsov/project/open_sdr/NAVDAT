#include "detection.h"
#include "NAVDAT/Modem/ofdmSyncImage.h"

#include <sdr_math.h>
#include <sdr_fft.h>

Sample_t navdat_detect_signal(Sample_t* fftMagnitude, Size_t Count)
{
  SDR_ASSERT(Count >= NAVDAT_OFDM_SYNCIMAGE_SIZE);
  Sample_t X = correlation(fftMagnitude, NAVDAT_OFDM_SYNCIMAGE_SIZE, NAVDAT_ofdmDetectSyncImage);
  return X;
}

static void collector(NAVDAT_SignalDetector_t* This, iqSample_t* Samples, Size_t Count, Bool_t isFirst)
{
  fft_iq(Samples, Count, This->fftBuf.P);
  for (Size_t i = 0; i < Count; ++i)
  {
    Sample_t mag = iq_magnitude_1(&This->fftBuf.P[i]);
    if (isFirst)
      This->magBuf.P[i] = mag;
    else
     This->magBuf.P[i] = ((Calc_t)This->magBuf.P[i] + mag)/2;
  }
}

Bool_t navdat_signal_detector(NAVDAT_SignalDetector_t* This, iqSample_t* Samples, Size_t Count)
{
  clear_buffer(This->magBuf.P, This->magBuf.Size);

  Size_t Counter = 0, fftSize = This->fftBuf.Size;
  Bool_t isFirst = 1;
  while (Counter < Count)
  {
    collector(This, &Samples[Counter],fftSize, isFirst);

    Counter += This->Step;
    if (isFirst) isFirst = 0;
  }
  This->Current = navdat_detect_signal(This->magBuf.P, Count);
  return This->Current >= This->Treshold;
}

void init_NAVDAT_SignalDetector(NAVDAT_SignalDetector_t* This, NAVDAT_SignalDetectorCfg_t* Cfg)
{
  This->Treshold = Cfg->Treshold;
  This->Step = Cfg->Step;

  SDR_ASSERT(Cfg->fftBuf.Size == Cfg->magBuf.Size);
  This->fftBuf = Cfg->fftBuf;
  This->magBuf = Cfg->magBuf;
}
