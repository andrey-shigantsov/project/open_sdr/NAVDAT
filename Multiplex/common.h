/*!
 * \file common.h
 * \author Шиганцов А.А.
 * \brief Общие елементы для мультиплекса NAVDAT
 */

#ifndef __NAVDAT_MULTIPLEX_COMMON_H__
#define __NAVDAT_MULTIPLEX_COMMON_H__

#include "../Modem/common.h"
#include "../Modem/dataCount.h"
#include "../Codec/common.h"

#ifdef __cplusplus
extern "C"{
#endif

#define NAVDAT_PACKET_HEAD_SIZE 4
#define NAVDAT_FILENAME_LEN 255
#define NAVDAT_FILE_FILTER_LEN 255

typedef struct
{
  UInt8_t year, mounth, day;
} NAVDAT_Date_t;
typedef struct
{
  UInt8_t hour, minute, sec;
} NAVDAT_Time_t;

typedef struct
{
  NAVDAT_TIS_qamMode_t tisMode;
  NAVDAT_DS_qamMode_t dsMode;

  UInt8_t misData[NAVDAT_MIS_BYTES_COUNT];
  UInt8_t tisData[NAVDAT_TIS16_BYTES_COUNT];
  UInt8_t dsData[NAVDAT_DS64_BYTES_COUNT];

  NAVDAT_DataCounter_t Count;
} NAVDAT_RawFrame_t;

typedef struct
{
  UInt8_t idFile; UInt16_t id; /* начинаются с 0 */
  UInt8_t flags;
  UInt16_t timestamp; // msec

  /* Присутствует в первом пакете */
  //{
  char* FileName;
  UInt32_t mmsi;
  //}

  byteBuffer_t data;
} NAVDAT_Frame_t;

INLINE Size_t NAVDAT_misRawCountMax(){return NAVDAT_MIS_BYTES_COUNT;}
INLINE Size_t NAVDAT_tisRawCountMax(NAVDAT_TIS_qamMode_t mode)
{
  Size_t size;
  switch (mode)
  {
  default:
  case NAVDAT_TIS_qam4:
    size = NAVDAT_TIS4_BYTES_COUNT;
    break;

  case NAVDAT_TIS_qam16:
    size = NAVDAT_TIS16_BYTES_COUNT;
    break;
  }
  return size;
}
INLINE Size_t NAVDAT_dsRawCountMax(NAVDAT_DS_qamMode_t mode)
{
  Size_t size;
  switch (mode)
  {
  default:
  case NAVDAT_DS_qam4:
    size = NAVDAT_DS4_BYTES_COUNT;
    break;

  case NAVDAT_DS_qam16:
    size = NAVDAT_DS16_BYTES_COUNT;
    break;

  case NAVDAT_DS_qam64:
    size = NAVDAT_DS64_BYTES_COUNT;
    break;
  }
  return size;
}

INLINE Size_t NAVDAT_misCountMax(NAVDAT_xIS_Code_t code){return NAVDAT_Code_xisCountMax(code, NAVDAT_misRawCountMax());}
INLINE Size_t NAVDAT_tisCountMax(NAVDAT_TIS_qamMode_t mode, NAVDAT_xIS_Code_t code){return NAVDAT_Code_xisCountMax(code, NAVDAT_tisRawCountMax(mode));}
INLINE Size_t NAVDAT_dsCountMax(NAVDAT_DS_qamMode_t mode, NAVDAT_DS_Code_t code){return NAVDAT_Code_dsCountMax(code, NAVDAT_dsRawCountMax(mode));}

Bool_t NAVDAT_FrameToData(NAVDAT_RawFrame_t* Frame, NAVDAT_qamData_t* Data);

Bool_t NAVDAT_misBufToFrame(qamBuffer_t *Data, NAVDAT_RawFrame_t *Frame);
Bool_t NAVDAT_tisBufToFrame(NAVDAT_TIS_qamMode_t Mode, qamBuffer_t *Data, NAVDAT_RawFrame_t *Frame);
Bool_t NAVDAT_dsBufToFrame(NAVDAT_DS_qamMode_t Mode, qamBuffer_t *Data, NAVDAT_RawFrame_t *Frame);

INLINE Bool_t NAVDAT_frameIsFirst(NAVDAT_Frame_t* frame)
{
  return frame->flags & 1;
}
INLINE Bool_t NAVDAT_frameIsLast(NAVDAT_Frame_t* frame)
{
  return (frame->flags >> 1) & 1;
}

#ifdef __cplusplus
}
#endif

#endif // __NAVDAT_MULTIPLEX_COMMON_H__
