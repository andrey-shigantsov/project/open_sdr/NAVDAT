/*!
 * \file qamConstellations.h
 * \author Шианцов А.А.
 * \brief Объявления QAM созвездий NAVDAT
 */

#include "SDR/BASE/Modulation/digital/qamModulation.h"

extern QAM_Constellation_t NAVDAT_DRM_QAM4_Constellation;
extern QAM_Constellation_t NAVDAT_DRM_QAM16_Constellation;
extern QAM_Constellation_t NAVDAT_DRM_QAM64_Constellation;
